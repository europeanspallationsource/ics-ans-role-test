import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_conda_user(User):
    user = User('conda')
    assert user.exists
    assert user.group == 'users'
    assert user.home == '/home/conda'
    assert user.shell == '/bin/bash'
    assert user.expiration_date is None


def test_conda_version(Command):
    cmd = Command('/opt/conda/bin/conda --version 2>&1')
    assert cmd.rc == 0
    assert cmd.stdout.strip() == 'conda 4.3.11'


def test_conda_path(Command):
    cmd = Command('su -l -c "echo \$PATH" conda')
    assert cmd.stdout.startswith('/opt/conda/bin:')
