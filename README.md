ics-ans-role-test
=================

Ansible role to test Jenkins job with Docker.

Requirements
------------

- ansible >= 2.2
- molecule >= 1.20

License
-------

BSD 2-clause
