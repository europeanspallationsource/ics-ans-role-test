---
- name: check if conda is installed
  shell: >
    /opt/conda/bin/conda --version 2>&1 | grep -qw {{conda_version}}
  check_mode: no
  failed_when: False
  changed_when: False
  register: is_conda_installed

- name: is conda installed
  debug:
    msg: "conda {{conda_version}} is installed: {{is_conda_installed.rc == 0}}"

- name: install required packages
  yum:
    name: "{{item}}"
    state: present
  with_items:
    - bzip2
    - sudo

- name: create conda user
  user:
    name: conda
    comment: "conda user"
    uid: "{{conda_uid | default(omit)}}"
    group: users
    shell: /bin/bash
    home: /home/conda
    system: yes

- name: delete previous conda installation
  file:
    path: /opt/conda
    state: absent
  when: is_conda_installed.rc != 0

- name: create conda directory
  file:
    path: /opt/conda
    state: directory
    mode: 0755
    owner: conda
    group: users

- name: download miniconda installer
  get_url:
    url: https://repo.continuum.io/miniconda/{{miniconda_installer}}
    checksum: md5:{{miniconda_installer_md5}}
    dest: /opt/conda
    owner: conda
    group: users
    mode: 0644
  when: is_conda_installed.rc != 0

- name: install conda
  command: >
    bash /opt/conda/{{miniconda_installer}} -p /opt/conda -b -f
  become: yes
  become_user: conda
  when: is_conda_installed.rc != 0

- name: delete miniconda installer
  file:
    path: /opt/conda/{{miniconda_installer}}
    state: absent

- name: copy conda_env.sh file
  copy:
    src: conda_env.sh
    dest: /etc/profile.d/conda_env.sh
    owner: root
    group: root
    mode: 0644

- name: copy condarc file
  copy:
    src: condarc
    dest: /home/conda/.condarc
    owner: conda
    group: users
    mode: 0644
